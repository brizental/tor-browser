/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at htp://mozilla.org/MPL/2.0/. */

import { html, ifDefined } from "../vendor/lit.all.mjs";
import { MozLitElement } from "../lit-utils.mjs";
// eslint-disable-next-line import/no-unassigned-import
import "chrome://global/content/elements/moz-label.mjs";

/**
 * A simple toggle element that can be used to switch between two states.
 *
 * @tagname moz-toggle
 * @property {boolean} pressed - Whether or not the element is pressed.
 * @property {boolean} disabled - Whether or not the element is disabled.
 * @property {string} label - The label text.
 * @property {string} description - The description text.
 * @property {string} ariaLabel
 *  The aria-label text for cases where there is no visible label.
 * @slot support-link - Used to append a moz-support-link to the description.
 * @fires toggle
 *  Custom event indicating that the toggle's pressed state has changed.
 */
export default class MozToggle extends MozLitElement {
  static shadowRootOptions = {
    ...MozLitElement.shadowRootOptions,
    delegatesFocus: true,
  };

  static properties = {
    pressed: { type: Boolean, reflect: true },
    disabled: { type: Boolean, reflect: true },
    label: { type: String },
    description: { type: String },
    ariaLabel: { type: String, attribute: "aria-label" },
    accessKey: { type: String, attribute: "accesskey" },
    // Extension for tor-browser. Used for tor-browser#41333.
    title: { type: String, attribute: "title" },
    // Extension for tor-browser. Used for tor-browser#40837.
    labelAlignAfter: { type: Boolean, attribute: "label-align-after" },
  };

  static get queries() {
    return {
      buttonEl: "#moz-toggle-button",
      labelEl: "#moz-toggle-label",
      descriptionEl: "#moz-toggle-description",
    };
  }

  // Use a relative URL in storybook to get faster reloads on style changes.
  static stylesheetUrl = window.IS_STORYBOOK
    ? "./moz-toggle/moz-toggle.css"
    : "chrome://global/content/elements/moz-toggle.css";

  constructor() {
    super();
    this.pressed = false;
    this.disabled = false;
  }

  handleClick() {
    this.pressed = !this.pressed;
    this.dispatchOnUpdateComplete(
      new CustomEvent("toggle", {
        bubbles: true,
        composed: true,
      })
    );
  }

  // Delegate clicks on the host to the input element
  click() {
    this.buttonEl.click();
  }

  labelTemplate() {
    if (this.label) {
      return html`
        <span class="label-wrapper">
          <label
            is="moz-label"
            id="moz-toggle-label"
            part="label"
            for="moz-toggle-button"
            accesskey=${ifDefined(this.accessKey)}
          >
            ${this.label}
          </label>
          ${!this.description ? this.supportLinkTemplate() : ""}
        </span>
      `;
    }
    return "";
  }

  descriptionTemplate() {
    if (this.description) {
      return html`
        <p
          id="moz-toggle-description"
          class="description-wrapper"
          part="description"
        >
          ${this.description} ${this.supportLinkTemplate()}
        </p>
      `;
    }
    return "";
  }

  supportLinkTemplate() {
    return html` <slot name="support-link"></slot> `;
  }

  render() {
    const { pressed, disabled, description, ariaLabel, handleClick } = this;
    // For tor-browser, if we have a title we use it as the aria-description.
    // Used for tor-browser#41333.
    // Only set the description using the title if it differs from the
    // accessible name derived from the label.
    const label = ariaLabel || this.label;
    const ariaDescription = label === this.title ? undefined : this.title;
    // For tor-browser, we want to be able to place the label after the toggle
    // as well.
    // Used for the enable-bridges switch tor-browser#40837.
    const labelAlignAfter = this.labelAlignAfter;
    return html`
      <link rel="stylesheet" href=${this.constructor.stylesheetUrl} />
      ${labelAlignAfter ? "" : this.labelTemplate()}
      <button
        id="moz-toggle-button"
        part="button"
        type="button"
        class="toggle-button"
        ?disabled=${disabled}
        aria-pressed=${pressed}
        aria-label=${ifDefined(ariaLabel ?? undefined)}
        aria-description=${ifDefined(ariaDescription ?? undefined)}
        aria-describedby=${ifDefined(
          description ? "moz-toggle-description" : undefined
        )}
        @click=${handleClick}
      ></button>
      ${labelAlignAfter ? this.labelTemplate() : ""}
      ${this.descriptionTemplate()}
    `;
  }
}
customElements.define("moz-toggle", MozToggle);
